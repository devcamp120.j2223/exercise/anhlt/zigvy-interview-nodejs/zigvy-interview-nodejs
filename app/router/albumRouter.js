//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { albumMiddleware } = require('../middleware/albumMiddleware');


//Import Controller
const { createAlbum, getAllAlbum, getAlbumById, updateAlbumByID, deleteAlbumById, getAlbumsOfUser } = require('../controller/albumController');

//Tạo router
const albumRouter = express.Router();

//Sử dụng middleware
albumRouter.use(albumMiddleware);


//Khai báo API

//CREATE album
albumRouter.post('/albums', createAlbum);


// GET ALL album
albumRouter.get('/albums', getAllAlbum);


//GET album BY ID
albumRouter.get('/albums/:albumId', getAlbumById);


//UPDATE album
albumRouter.put('/albums/:albumId', updateAlbumByID);


//DELETE album
albumRouter.delete('/albums/:albumId', deleteAlbumById);


//GET albumS OF USER
albumRouter.get('/users/:userId/albums', getAlbumsOfUser);

module.exports = albumRouter;