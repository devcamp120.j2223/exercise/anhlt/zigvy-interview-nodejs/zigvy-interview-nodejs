//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { todoMiddleware } = require('../middleware/todoMiddleware');

//Import Controller
const { createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodosOfUser } = require('../controller/todoController');

//Tạo router
const todoRouter = express.Router();

//Sử dụng middleware
todoRouter.use(todoMiddleware);


// KHAI BÁO API

//   CREATE TODO
todoRouter.post('/todos', createTodo);


//GET ALL TODO
todoRouter.get('/todos', getAllTodo);


//GET TODO BY ID
todoRouter.get('/todos/:todoId', getTodoById);


//UPDATE TO BY ID
todoRouter.put('/todos/:todoId', updateTodoById);


// DELETE TODO BY ID
todoRouter.delete('/todos/:todoId', deleteTodoById);



// GET  TODO  OF USER
todoRouter.get('/users/:userId/todos', getTodosOfUser);

//EXPORT
module.exports = todoRouter;