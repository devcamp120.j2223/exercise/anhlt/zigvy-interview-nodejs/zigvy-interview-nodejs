//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { userMiddleware } = require('../middleware/userMiddleware');


//Import Controller
const { createUser, getAllUser, getUserById, updateUserByID, deleteUserById } = require('../controller/userController');
//Tạo router
const userRouter = express.Router();

//Sử dụng middleware
userRouter.use(userMiddleware);


//Khai báo API

//CREATE USER
userRouter.post('/users', createUser);


// GET ALL USER
userRouter.get('/users', getAllUser);


//GET USER BY ID
userRouter.get('/users/:userId/post', getUserById);


//UPDATE USER
userRouter.put('/users/:userId', updateUserByID);


//DELETE USER
userRouter.delete('/users/:userId', deleteUserById);

// EXPORT
module.exports = userRouter;