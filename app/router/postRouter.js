//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { postMiddleware } = require('../middleware/postMiddleware');


//Import Controller
const { createPost, getAllPost, getPostById, updatePostByID, deletePostById, getPostsOfUser } = require('../controller/postController');

//Tạo router
const postRouter = express.Router();

//Sử dụng middleware
postRouter.use(postMiddleware);


//Khai báo API

//CREATE POST
postRouter.post('/posts', createPost);


// GET ALL POST
postRouter.get('/posts', getAllPost);


//GET POST BY ID
postRouter.get('/posts/:postId', getPostById);


//UPDATE POST
postRouter.put('/posts/:postId', updatePostByID);


//DELETE POST
postRouter.delete('/posts/:postId', deletePostById);


//GET POSTS OF USER
postRouter.get('/users/:userId/posts', getPostsOfUser);

module.exports = postRouter;