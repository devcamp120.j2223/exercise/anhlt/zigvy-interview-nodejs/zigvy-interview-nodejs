// Import Model
const postModel = require('../model/postModel');

//  Khai báo mongoose
const mongoose = require('mongoose');

//CREATE A POST
const createPost = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu

    if (!bodyRequest.userId) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }

    if (!bodyRequest.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }

    if (!bodyRequest.body) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Body Is required",
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let createPost = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body
    }
    postModel.create(createPost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create POST success",
                data: data
            })
        }
    })
}


//GET ALL POST
const getAllPost = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.query.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }
    //B2: validate dữ liệu

    //B3: thao tác với cơ sở dữ liệu
    postModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get ALL POST",
                data: data
            })
        }
    })
}

//GET POST BY ID
const getPostById = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    postModel.findById(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Post by Id successfully",
                data: data
            })
        }
    })
}

//UPDATE POST
const updatePostByID = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updatePost = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body
    }
    postModel.findByIdAndUpdate(postId, updatePost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Update Post successfully",
                data: data
            })
        }
    })
}


// DELETE POST
const deletePostById = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not a valid"
        })
    }

    // B3: thao tác với cơ sở dữ liệu
    postModel.findByIdAndDelete(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete POST successfully"
            })
        }
    })

}


//GET POSTS OF USER
const getPostsOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    postModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Post Of User Id: " + userId,
                data: data
            })
        }
    })
}


// EXPORT
module.exports = { createPost, getAllPost, getPostById, updatePostByID, deletePostById, getPostsOfUser }