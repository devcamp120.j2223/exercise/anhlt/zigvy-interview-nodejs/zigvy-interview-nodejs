// Import Model
const photoModel = require('../model/photoModel');

//  Khai báo mongoose
const mongoose = require('mongoose');

//CREATE A PHOTO
const createPhoto = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.albumId) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }

    if (!bodyRequest.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }

    if (!bodyRequest.url) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Url Is required",
        })
    }

    if (!bodyRequest.thumbnailUrl) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Thumbnail Url Is required",
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let createPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }
    photoModel.create(createPhoto, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create Photo success",
                data: data
            })
        }
    })
}


//GET ALL Photo
const getAllPhoto = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.query.albumId;

    let condition = {};

    if (albumId) {
        condition.albumId = albumId;
    }
    //B2: validate dữ liệu

    //B3: thao tác với cơ sở dữ liệu
    photoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get ALL Photo",
                data: data
            })
        }
    })
}

//GET Photo BY ID
const getPhotoById = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    photoModel.findById(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Photo by Id successfully",
                data: data
            })
        }
    })
}

//UPDATE Photo
const updatePhotoByID = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updatePhoto = {
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(photoId, updatePhoto, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Update Photo successfully",
                data: data
            })
        }
    })
}


// DELETE Photo
const deletePhotoById = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not a valid"
        })
    }

    // B3: thao tác với cơ sở dữ liệu
    photoModel.findByIdAndDelete(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Photo successfully"
            })
        }
    })

}


//GET PhotoS OF USER
const getPhotosOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;

    let condition = {};

    if (albumId) {
        condition.albumId = albumId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    photoModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Photo Of User Id: " + albumId,
                data: data
            })
        }
    })
}


// EXPORT
module.exports = { createPhoto, getAllPhoto, getPhotoById, updatePhotoByID, deletePhotoById, getPhotosOfUser }